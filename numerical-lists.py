# for number in range(1,21):
#    print(number)

# numbers = [number for number in range (1,1000001)]
# print('Minimum = {}'.format(min(numbers)))
# print('Maximum = {}'.format(max(numbers)))
# print('Sum = {}'.format(sum(numbers)))

# odds = [odd for odd in range(1,21,2)]
# for number in odds:
#    print(number)

# multiples = [base*3 for base in range(1,11)]
# for number in multiples:
#    print(number)

cubes = [base**3 for base in range(1,11)]
for number in cubes:
   print(number)
